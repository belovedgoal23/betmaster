'''
author:beloved
'''

from cwc import CWC
from conf import *
import os
import logging
import pika
import json
from betmaster import task
periodLength = 7

fmt = '[%(asctime)s] %(message)s'
#log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'worker.log')
log = "/var/log1/smsgw/worker.log"
#pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'run.pid')
logging.basicConfig(filename=log, level=logging.DEBUG, format=fmt)

SERVICE = {'8103': '1', '8104': '2'}


def callback(ch, method, properties, body):
    '''

    :param ch:
    :param method:
    :param properties:
    :param body:
    :return:
    '''
    logging.debug("Received for process %s" % body)
    subscription =json.loads(body)
    process(subscription)


def process_queue():
    '''

    :return:
    '''
    credentials = pika.PlainCredentials('bet', 'london')
    connection = pika.BlockingConnection(pika.ConnectionParameters('69.64.81.160', 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue='betmaster')

    channel.basic_consume(callback,
                          queue='betmaster',
                          no_ack=True)
    print('[*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


def connection():
    con = CWC(PG_HOST, PG_USER, PG_DB, PG_PASSWORD)
    return con.connect_to_pg()


def process(user_dict):
    '''

    :param user_dict:
    :return:
    '''
    #print(user_dict)
    update_reason = ''
    msisdn = user_dict['msisdn']
    service_id = SERVICE[user_dict['productId']]
    charge_time = user_dict['chargigTime']
    price = int(float((user_dict['amount'])))
    errorCode = user_dict['errorCode']
    errorMsg = user_dict['errorMsg']
    reason = errorMsg

    try:
        if errorCode == '1000':
            task.get_daily_odds('234%s' % msisdn[-10:])
            if check_exist(service_id, msisdn):
                reactivate_user(msisdn, service_id)
                insert_cdr(msisdn, service_id, reason, price)
                update_reason = errorMsg
            else:
                insert_user(msisdn, service_id)
                insert_cdr(msisdn, service_id, reason, price)
                update_reason = errorMsg 
            #task.get_daily_odds(msisdn)
        elif errorCode == '1001':
            logger.info('deactivating user %s ' % msisdn) 
            deactivate_user(msisdn, service_id)
            #insert_cdr(msisdn, service_id, reason, price)
        else:
            logging.info("%s option received" % errorMsg)
            update_reason = errorMsg
    except Exception as ex:
        update_reason = "Internal Error"
        logging.error("An erro occured %s" % str(ex))
    finally:
        logging.info('Error logs')
        insert_logs(msisdn,service_id,update_reason, price)
    #return 'processed'


def insert_user(msisdn, service_id):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "Insert into Users(MSISDN,SERVICE_ID,FIRST_REG_TIME,LAST_REG_TIME,EFFECTIVE_TIME,EXPIRY_TIME,CHANNEL,STATUS)"  \
              "values('%s','%s',current_timestamp,current_timestamp,current_timestamp,current_timestamp+interval '%s' day, 'API', 1)" % (msisdn, service_id, periodLength)
        logging.debug("Insert user:%s" %  sql)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def reactivate_user(msisdn, service_id):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        channel = 'API'
        sql = "update Users set STATUS=1, LAST_REG_TIME = current_timestamp, EFFECTIVE_TIME = current_timestamp," \
              "EXPIRY_TIME = current_timestamp+interval '%s' day, channel='%s' where service_id=%s and msisdn='%s'" % (periodLength, channel, service_id, msisdn)
        logging.debug("Reactivate user: %s" % sql)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("Reactivate user error occured: %s" % str(ex))


def deactivate_user(msisdn, service_id):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "update Users set STATUS=0,LAST_UNREG_TIME = current_timestamp where service_id='%s' and msisdn='%s'" % (service_id, msisdn)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def insert_cdr(msisdn, service_id, reason, price):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        channel = 'API'
        result = 1
        description = 'SuccessFul'
        sql = "insert into cdr(service_id,charge_time,msisdn,price,reason,channel,result,description) " \
              "values('%s',current_timestamp,'%s','%s','%s','%s','%s','%s')" % (service_id, msisdn, price, reason, channel, result, description)
        logging.info("Insert CDR: %s" % sql)
        res = cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("Insert CDR ERROR: %s" % str(ex))


def insert_logs(msisdn, service_id, reason, price):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        result = 1
        description = 'SuccessFul'
        sql = "insert into register_logs(service_id,msisdn,request_type,message,channel,result,description,created_time,updated_time)" \
              " values('%s', '%s', '%s', '%s', '%s', '%s', '%s',current_timestamp,current_timestamp)" % (service_id, msisdn, 1, 1, "", result, description)
        logging.info("InsertLog: %s" % sql)
        res  = cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def check_exist(service_id, msisdn):
    '''

    :param service_id:
    :param msisdn:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "Select status from users where service_id = '%s' and msisdn = '%s'" % (service_id, msisdn)
        logging.info("Check Exist Sql: %s" % sql)
        cursor.execute(sql)
        res = cursor.fetchone()
        if res is None:
            return False
        return True
    except Exception as ex:
        logging.error("Check exist error occured: %s" % str(ex))

''' worker main start up with rabbitMqueue'''
process_queue()
#print check_exist('16', '2348022221569')
