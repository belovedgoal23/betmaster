
non_ht_xml = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sub="http://SubscriptionEngine.ibm.com">
               <soapenv:Header/>
               <soapenv:Body>
                  <sub:handleNewSubscription>
                   <custAttributesDTO>
                    <cpId>{{ cpid }}</cpId>
                    <cpPwd>{{ pwd }}</cpPwd>
                    <msisdn>{{ msisdn }}</msisdn>
                    <channelName>{{ channel }}</channelName>
                    <productId>{{ product_id }}</productId>
                    <cpName>{{cpname }}</cpName>
                    <aocMsg1>1</aocMsg1>
                    <aocMsg2>9</aocMsg2>
                   <firstConfirmationDTTM>2019-07-15T10:40:28Z</firstConfirmationDTTM>
                    </custAttributesDTO>
                  </sub:handleNewSubscription>
               <sub:handleHTNewActivation><activationRequest><cpID/><songProdID/>
            <subProdID/></activationRequest></sub:handleHTNewActivation></soapenv:Body>
            </soapenv:Envelope>
            """
#TIME = 2019-07-15T10:40:28Z<

otc_xml = """
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sub="http://SubscriptionEngine.ibm.com">
               <soapenv:Header/>
               <soapenv:Body>
                  <sub:handleNewSubscription>
                   <custAttributesDTO>
                       <cpId>{{ cpid }}</cpId>
                        <cpPwd>{{ pwd }}</cpPwd>
                        <msisdn>{{ msisdn }}</msisdn>
                        <channelName>{{ channel }}</channelName>
                        <productId>{{ product_id }}</productId>
                        <cpName>{{ cpname }}</cpName>
                        <productId>8104</productId>
                        <param_1>OTC</param_1>
                    <aocMsg1>1</aocMsg1>
                    <aocMsg2>9</aocMsg2>
            <firstConfirmationDTTM>2019-07-04T15:23:03.890Z</firstConfirmationDTTM>
                    </custAttributesDTO>
                  </sub:handleNewSubscription>
               <sub:handleHTNewActivation><activationRequest><cpID/><songProdID/>
            <subProdID/></activationRequest></sub:handleHTNewActivation></soapenv:Body>
            </soapenv:Envelope>
             """