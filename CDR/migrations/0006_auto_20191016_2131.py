# Generated by Django 2.2.2 on 2019-10-16 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CDR', '0005_users'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cdr',
            name='reason',
            field=models.CharField(max_length=1000),
        ),
    ]
