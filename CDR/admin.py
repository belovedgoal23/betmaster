from django.contrib import admin
from CDR.models import *
from django.http import HttpResponse
from django.db.models import Q
import csv

# Register your models here.


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('code', 'package_code', 'name', 'schedule', 'price', 'period_length', 'free_length', 'status', 'created_time')


class RegisterLogAdmin(admin.ModelAdmin):
    list_display = ('service', 'msisdn', 'request_type', 'channel', 'result', 'message', 'description', 'created_time')
    actions = ['download_msisdns', ]

    def download_msisdns(self, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename=UniqueMSISDNs.csv'
        writer = csv.writer(response)
        queryset = RegisterLog.objects.distinct("msisdn")
        writer.writerows([[obj.msisdn[-10:]] for obj in queryset])
        return response


class UserAdmin(admin.ModelAdmin):
    list_display = ('service', 'msisdn', 'first_reg_time', 'last_reg_time', 'last_unreg_time', 'last_renew_time', 'last_reply_time', 'effective_time','expiry_time','status')
    list_filter = ('status', 'service')
    search_fields = ('msisdn',)


class CdrAdmin(admin.ModelAdmin):
    list_display = ('service', 'msisdn', 'price', 'reason', 'channel', 'description', 'result', 'charge_time')
    search_fields = ('msisdn', 'result')
    list_filter = ('service', 'channel', 'charge_time', 'reason')

    actions = ['download_selected', ]

    def download_selected(self, request, queryset):
        headers = ['service', 'msisdn', 'price', 'reason', 'channel', 'description', 'result', 'charge_time']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;filename=CDR.csv'
        writer = csv.writer(response)
        queryset = queryset.filter(Q(reason="AOC: Success") | Q(reason="Success"))
        writer.writerow([header.upper() for header in headers])
        for obj in queryset:
            line = []
            for header in headers:
                line.append(getattr(obj, header))
            writer.writerow(line)
        return response


class SuccessfulCdrAdmin(admin.ModelAdmin):
    list_display = ('service', 'msisdn', 'price', 'reason', 'channel', 'result', 'charge_time')
    search_fields = ('msisdn', 'result')
    list_filter = ('service', 'channel', 'reason', 'charge_time')




admin.site.register(Service, ServiceAdmin)
admin.site.register(RegisterLog, RegisterLogAdmin)
admin.site.register(Users, UserAdmin)
admin.site.register(Cdr, CdrAdmin)
admin.site.register(SuccessfulCdr, SuccessfulCdrAdmin)
