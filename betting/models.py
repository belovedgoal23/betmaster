from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.

from django.core.exceptions import ObjectDoesNotExist

class Betting(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    daily_odds = models.CharField(max_length=11, null=False, blank=False)
    message = models.TextField()
    odds_date = models.DateField(null=False, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)

    #class Meta:
    #    unique_together=['odds_date']
    def __unicode__(self):
        return self.name

    def is_available(self):
        try:
            return Betting.objects.get(odds_date=self.odds_date)
        except ObjectDoesNotExist:
            return False
 
    def clean(self):
        if self.is_available():
            raise ValidationError('Item already booked for those dates')


class Request(models.Model):
    msisdn = models.CharField(max_length=50, null=True, blank=True)
    plan = models.CharField(max_length=11, null=True, blank=True)
    session_id = models.CharField(max_length=20, null=True, blank=True)
    status = models.BooleanField(null=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.msisdn
