from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
import xml.etree.ElementTree as ET
import logging,os
import conf
from script.Subscription import *
import json
from django.db import connection
from activation import *
from .models import UssdRequest

#http://69.64.81.160:9889/bet/airtel/subscription?msisdn=2348021206395&service_id=8103&channel=sms&type=otcff


def process_response(soap_xml):
    '''
    :param soap_xml:
    :return:
    '''
    def soap_process(soap_xml):
        tree = ET.fromstring(soap_xml)
        response = {key: tree[1][0][0].find(key).text for key in conf.keys}
        logging.info(response)
        return response
    return soap_process(soap_xml)


def bet_daily(service_id, msisdn, channel):
    sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
    xml_sub_res = sub.subscribe_otc(service_id, msisdn, channel, conf.otc_xml)
    res = process_response(xml_sub_res.text)
    logging.debug(res)
    return res


def bet_weekly(service_id, msisdn, channel):
    sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
    xml_sub_res = sub.subscribe_non_ht(service_id, msisdn, channel, conf.non_ht_xml)
    res = process_response(xml_sub_res.text)
    logging.debug(res)
    return res


def insert_user(con, msisdn):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        sql = "Insert into Users(MSISDN,SERVICE_ID,FIRST_REG_TIME,LAST_REG_TIME,EFFECTIVE_TIME,EXPIRY_TIME,CHANNEL,STATUS)"  \
              "values('%s','%s',current_timestamp,current_timestamp,current_timestamp,current_timestamp+interval '%s' day, 'API', 1)" % (msisdn, 1, 1)
        logging.debug("Insert user:%s" %  sql)
        con.execute(sql)
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


class Ussd(View):
    #AirtelUssd / MFill
    #USSD_LIST = (1:)
    def get(self, requests):
        msisdn = requests.GET['msisdn']
        ussd_input = requests.GET['INPUT']
        session_id = requests.GET['sessionid']
        _save_request = UssdRequest.objects.create(msisdn=msisdn, session_id=session_id)
        return self.get_user_input(msisdn=msisdn, ussd_input=ussd_input)

    def get_user_input(self, **kwargs):
        input = kwargs['ussd_input']
        msisdn = kwargs['msisdn']

        if input == "20035":
            message = "Welcome to betmaster.\n"
            message += "1. Daily bet odds \n"
            message += "2. Weekly bet odss \n"
            response = HttpResponse(message)
            response['Freeflow'] = "FC"
        elif input in ("1", "20035*1"):
            cursor = connection.cursor()
            se_ussd_response = bet_daily(conf.PID_DAILY, msisdn, 'ussd')
            logging.debug("Response receive %s" % se_ussd_response['errorMsg'])
            if se_ussd_response['errorCode'] == "1000":
                message = "Your request was successful,  sms with odds will be sent shortly."
                insert_user(cursor, msisdn)
                insert_cdr(msisdn, conf.PID_DAILY, se_ussd_response['errorMsg'], se_ussd_response['amount'])
                insert_logs(msisdn, conf.PID_DAILY, se_ussd_response['errorMsg'], se_ussd_response['amount'])
            else:
                message = se_ussd_response['errorMsg']
                insert_cdr(msisdn, conf.PID_DAILY, se_ussd_response['errorMsg'], se_ussd_response['amount'])
                insert_logs(msisdn, conf.PID_DAILY, se_ussd_response['errorMsg'], se_ussd_response['amount'])
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
        elif input in ("2", "20035*2"):
            se_ussd_response = bet_weekly(conf.PID_WEEKLY, msisdn, 'ussd')
            logging.debug("Response receive %s" % se_ussd_response['errorMsg'])
            message = "Your request is being processed!."
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
        else:
            message = "Welcome to betmaster.\n"
            message += "1. Daily bet odds \n"
            message += "2. Weekly bet odss \n"
            response = HttpResponse(message)
            response['Freeflow'] = "FC"
        return response


