from django.http import HttpResponse
from django.views import View
from betmaster.task import *
from CDR.models import Users
from datetime import datetime
import json
import requests
import logging

logger = logging.getLogger(__name__)

#http://69.64.81.160:9889/bet/airtel/subscription?msisdn=2348021206395&service_id=8103&channel=sms&type=otcff

class MyView(View):
    def get(self, request, *args, **kwargs):
        debug_task.delay()
        return HttpResponse('Hello, World!')

def check_active_status(msisdn):
    count_active = Users.objects.filter(msisdn=msisdn, expiry_time__date__gte=date.today()).count()
    return True if count_active > 0 else False


class Ussd(View):
    def get(self, requests):
        """
        :param requests:
        :return:
        """
        print(requests)
        msisdn = requests.GET['msisdn']
        ussd_input = requests.GET['INPUT']
        sessionid = requests.GET['sessionid']
        return self.get_user_input(msisdn=msisdn, ussd_input=ussd_input, sessionid=sessionid)

    def get_user_input(self, **kwargs):
        """
        :param kwargs:
        :return:
        """
        input = kwargs['ussd_input']
        msisdn = kwargs['msisdn']
        sessionid = kwargs['sessionid']
        if input == "20035":
            message = "Welcome to betmaster.\n"
            message += "1. Daily bet odds \n"
            message += "2. Weekly bet odds \n"
            message += "3. Today's odds \n"
            response = HttpResponse(message)
            response['Freeflow'] = "FC"
        elif input in ("1", "20035*1"):
            _save_request = Request.objects.create(msisdn=msisdn, session_id=sessionid)
            message = "Your request has been received, you will get a confirmation via sms."
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
            subscribe_to_otc.delay(msisdn)
        elif input in ("7", "20035*7"):
            url = "http://127.0.0.1:9060/api/customer/wallet/balance?msisdn=%s" %  '0'+msisdn[-10:]
            print(url)
            res = requests.get(url)
            _new = json.loads(res.text)
            if 'wallet_balance' in _new.keys():
                 amount = _new['wallet_balance']
                 message = "Your have %s in your smartcash balance." %  amount 
            else:
                 message = "You do not have a smartcash account, please visit any airtel agent outlet to open an account"
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
        elif input in ("2", "20035*2"):
            _save_request = Request.objects.create(msisdn=msisdn, session_id=sessionid)
            message = "Your request has been received, you will get a confirmation via sms."
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
            subscribe_to_weekly.delay(msisdn)
        elif input in ("3", "20035*3"):
            _save_request = Request.objects.create(msisdn=msisdn, session_id=sessionid)
            message = "Your request has been received, you will get a confirmation via sms."
            response = HttpResponse(message)
            response['Freeflow'] = "FB"
            if check_active_status(msisdn):
                get_daily_odds(msisdn)
            else:
                push_sms(msisdn, "You do not have an active subscription, dial *20035*1# for daily plan and *20035*2# for weekly plan.")
        else:
            message = "Welcome to betmaster.\n"
            message += "1. Daily bet odds \n"
            message += "2. Weekly bet odss \n"
            message += "3. Today's odds \n"
            response = HttpResponse(message)
            response['Freeflow'] = "FC"
        return response


