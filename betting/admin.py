from django.contrib import admin
from betting.models import *
# Register your models here.


class BettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'daily_odds', 'message', 'odds_date', 'date_created')
    list_filter = ('odds_date', 'date_created')


admin.site.register(Betting, BettingAdmin)