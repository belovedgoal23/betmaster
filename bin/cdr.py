import logging
from datetime import date, datetime, timedelta
#import urllib, urllib2
#from urllib.request import urlopen
#from urllib.parse import urlencode
import sys,os
import django
from daemon import Daemon


# PID = '/var/log1/emailapproval/dip.pid'
PID = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'daily_cdr.pid')
LOGFILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'daily_cdr.log')
#LOGFILE = '/Users/oluwasemilore/projects/wicomnp/result.log'
FMT = '[%(asctime)s] %(levelname)s %(message)s'
logging.basicConfig(filename=LOGFILE, format=FMT, level=logging.DEBUG)
# sys.path.append(os.path.abspath("/Users/oluwasemilore/projects/emailapproval"))
sys.path.append(os.path.dirname(os.path.abspath('')))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "betmaster.settings")
django.setup()


MESSAGE ="Still not getting our winning bet odds because of low credit? Complete your subscription. Recharge N100 and dial *20035*2# now to start enjoying sure bet odds"
#MESSAGE = "Tired of losing bet? It's time to start winning with Betmaster Sure Banker Odds. Dial *20035# or click bit.ly/3bvrW8P to subscribe."
from betting.models import *
from CDR.models import Users, RegisterLog, Cdr
from activation import send_sms
from Billing import conf

WHITELIST =['2348022221569',]
IS_TEST = False



def check_active_status():
    sms_active_records = []
    active_records = Cdr.objects.distinct()
    active_records.count()
    for record in active_records:
        sms_active_records.append(record.msisdn)
    return set(sms_active_records)


def push_sms(msisdn, message):
    return send_sms(conf.SMS_URL, 'Betmaster', '234%s' % msisdn[-10:], message)


def send_daily_content(active_records):
     logging.info("Starting... records:%s" % active_records)
     for record in active_records:
         try:
             push_sms(record, MESSAGE)
             logging.info("Sent content to %s" % record)
         except Exception as ex:
             logging.error("An error occured %s" % str(ex))
             pass


class DailyContentDaemon(Daemon):
    def run(self):
        send_daily_content(check_active_status())
        #push_sms('2348022221569', MESSAGE)


if __name__ == '__main__':
    daemon = DailyContentDaemon(PID, stdout=LOGFILE, stderr=LOGFILE)
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.stop()
            daemon.start()
        else:
            print('unknown command')
            sys.exit(2)
        sys.exit(0)
    else:
        print ('usage: %s start|stop|restart' % sys.argv[0])
        sys.exit(2)

