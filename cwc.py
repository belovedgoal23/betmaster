import psycopg2
import os
import logging
from datetime import date, datetime
from daemon import Daemon
from conf import *
#import urllib, urllib2
from urllib.request import urlopen
from urllib.parse import urlencode, quote_plus
import sys


logger = logging.getLogger(__name__)

# #LOG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'emautils.log')
# #LOG_FILE = os.path.join('log', 'Loggile-%s.log' % date.today())
# LOG_FILE = '/var/log1/smsgw/SMSGW-%s.log' % date.today()
# #PID_FILE = '/var/log1/smsgw/SMSG.PID'
# logger = logging.getLogger('smsgw')
# handler = logging.FileHandler(LOG_FILE)
# formatter = logging.Formatter('[%(asctime)s] %(levelname)s %(message)s')
# handler.setFormatter(formatter)
# logger.addHandler(handler)
# logger.setLevel(logging.DEBUG)

#from=$sender&to=$recipient&text=$message&service=$service_id


class CWC():

    def __init__(self, host, user, db, password):
        self.pg_host = host
        self.pg_user = user
        self.pg_db = db
        self.pg_password = password
        #self.service_id = kwargs.get('service_id')

    def connect_to_pg(self):
        try:
           con = psycopg2.connect(host=self.pg_host, user=self.pg_user, password=self.pg_password, dbname=self.pg_db)
        except Exception as ex:
            logger.error('Connection failed, %s' % str(ex))
        else:
            return con

    def active_subscribers(self, service_id):
        _con = self.connect_to_pg()
        try:
            cur = _con.cursor()
            sql = "select msisdn from users join script on script.service_id = users.service_id and users.service_id = '%s'  and expiry_time >= '%s' " % (service_id, datetime.now())
            print (sql)
            sql = str(sql)
            logger.debug(sql)
            cur.execute(sql)
            res = cur.fetchall()
            cur.close()
            _con.close()
            result = []
            if res:
                 [result.append(i[0]) for i in res]
            logger.info('Count: %s for the service id %s' % (len(result), service_id))
            return result
        except Exception as ex:
            logger.error('An error occured %s', str(ex))


    def send_sms(self, sms_url, short_code, msisdn, service_id, msg):
        '''
        Send message to subscriber using short_code
        '''
        to = '234%s' % msisdn[-10:]
        params = {
                'from' :short_code,
                'text': msg,
                'to':'234%s' %  msisdn[-10:],
                'service': service_id
                }
        url = sms_url + urlencode(params)
        try:
            logger.info('%s, invoking: %s' % (msisdn, url))
            response = urlopen(url)
        except Exception as exc:
            logger.error('url: %s, error: %s' % (
                url, str(exc)))
        else:
            logger.info('msisdn: %s, response: %s' % (to, response.read()))


if __name__ == '__main__':
    pass
    #res = SMSGW(A_PG_HOST, A_PG_USER, A_PG_DB, A_PG_PASSWORD)
    #tep = res.active_subscribers(13)
    #print tep

