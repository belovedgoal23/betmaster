'''
author:beloved
'''

from cwc import CWC
from conf import *
import os
import logging
import pika
import json
from datetime import date, datetime
periodLength = 7

fmt = '[%(asctime)s] %(message)s'
log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'subscription.log')
#pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'run.pid')
logging.basicConfig(logfile=log, level=logging.DEBUG, format=fmt)

SERVICE = {'5803': '13', '5806': '16', '5797': '7', '5794': '4'}


def connection():
    con = CWC(PG_HOST, PG_USER, PG_DB, PG_PASSWORD)
    return con.connect_to_pg()


def insert_retry(msisdn, service_id, act_date, status):
    '''
    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "insert into pending_activations(msisdn,activation_date,product_id, status, date_created)" \
              " values('%s', '%s', '%s', '%s',current_timestamp)" % (msisdn, act_date, service_id,status)
        logging.info("InsertRequests: %s" % sql)
        res  = cursor.execute(sql)
        con.commit()
    except Exception, ex:
        logging.error("An error occured: %s" % str(ex))


def update_retry(ids):
    '''

    :param ids:
    :return:
    '''
    con = connection()
    cur = con.cursor()
    sql = "update pending_activations set count_t= count_t + 1 where id in (%s)" % ids
    cur.execute(sql)
    con.commit()


def retry_logs():
    '''
    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''

    con = connection()
    cursor = con.cursor()
    dt = date.today().strftime("%Y-%m-%d")
    sql = "select * from pending_activations where activation_date='%s' and status=1 and count_t < 4" % dt
    logging.info("retrieve pending customers: %s" % sql)
    cursor.execute(sql)
    res = [i for i in cursor.fetchall()]
    con.close()
    return res


def daily_activation():
    '''

    :return:
    '''
    con = connection()
    cursor = con.cursor()
    dt = date.today().strftime("%Y-%m-%d")
    sql = "select msisdn from cdr where  reason='REG' and charge_time > '%s'" % dt
    logging.info("retrieve pending customers: %s" % sql)
    cursor.execute(sql)
    d = [i[0] for i in cursor.fetchall()]
    con.close()
    return d


def insert_user(msisdn, service_id):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "Insert into Users(MSISDN,SERVICE_ID,FIRST_REG_TIME,LAST_REG_TIME,EFFECTIVE_TIME,EXPIRY_TIME,CHANNEL,STATUS)"  \
              "values('%s','%s',current_timestamp,current_timestamp,current_timestamp,current_timestamp+interval '%s' day, 'API', 1)" % (msisdn, service_id, periodLength)
        logging.debug("Insert user:%s" %  sql)
        cursor.execute(sql)
        con.commit()
    except Exception, ex:
        logging.error("An error occured: %s" % str(ex))


def insert_cdr(msisdn, service_id, reason, price):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        channel = 'API'
        result = 1
        description = 'SuccessFul'
        sql = "insert into cdr(service_id,charge_time,msisdn,price,reason,channel,result,description) " \
              "values('%s',current_timestamp,'%s','%s','%s','%s','%s','%s')" % (service_id, msisdn, price, reason, channel, result, description)
        logging.info("Insert CDR: %s" % sql)
        res = cursor.execute(sql)
        con.commit()
    except Exception, ex:
        logging.error("Insert CDR ERROR: %s" % str(ex))


def insert_logs(msisdn, service_id, reason, price):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        result = 1
        description = 'SuccessFul'
        sql = "insert into register_logs(service_id,msisdn,request_type,message,channel,result,description,created_time,updated_time)" \
              " values('%s', '%s', '%s', '%s', '%s', '%s', '%s',current_timestamp,current_timestamp)" % (service_id, msisdn, 1, 1, "", result, description)
        logging.info("InsertLog: %s" % sql)
        res  = cursor.execute(sql)
        con.commit()
    except Exception, ex:
        logging.error("An error occured: %s" % str(ex))


print daily_activation()