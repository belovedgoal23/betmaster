from __future__ import absolute_import
import os
import time
from script.Subscription import *
from activation import *
import xml.etree.ElementTree as ET
import logging
from datetime import date, datetime
from .celery import app
import conf
from django.core.exceptions import ObjectDoesNotExist

fmt = '[%(asctime)s] %(message)s'
#log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'celery.log')
log = "/var/log1/sms/celery.log"
logging.basicConfig(filename=log, level=logging.DEBUG, format=fmt)
import django
django.setup()

from betting.models import *


@app.task(bind=True)
def debug_task(self):
    #send_sms(conf.SMS_URL, '20035', '2348022221569', "Thanks")
    get_daily_odds('2348022221569')
    print('Request: {0!r}'.format(self.request))


def get_daily_odds(msisdn):
    _dt = date.today()
    try:
        _daily_odds = Betting.objects.get(odds_date=_dt)
        push_sms(msisdn, _daily_odds.message)
        logging.info("Daily Sms sent to the %s" % msisdn)
    except ObjectDoesNotExist:
        push_sms(msisdn, "Odds not yet available, you will receive an SMS soon")
        logging.info("NotAvailable Odds not sent to %s" % msisdn)


def push_sms(msisdn, message):
    return send_sms(conf.SMS_URL, '20035', '234%s' % msisdn[-10:], message)


def process_response(soap_xml):
    '''
    :param soap_xml:
    :return:
    '''
    def soap_process(soap_xml):
        tree = ET.fromstring(soap_xml)
        response = {key: tree[1][0][0].find(key).text for key in conf.keys}
        logging.info(response)
        return response
    return soap_process(soap_xml)


def bet_daily(service_id, msisdn, channel):
    """
    :param service_id:
    :param msisdn:
    :param channel:
    :return:
    """
    sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
    xml_sub_res = sub.subscribe_otc(service_id, msisdn, channel, conf.otc_xml)
    res = process_response(xml_sub_res.text)
    logging.debug(res)
    return res


def bet_weekly(service_id, msisdn, channel):
    """
    :param service_id:
    :param msisdn:
    :param channel:
    :return:
    """
    sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
    xml_sub_res = sub.subscribe_non_ht(service_id, msisdn, channel, conf.non_ht_xml)
    res = process_response(xml_sub_res.text)
    logging.debug(res)
    return res


@app.task(bind=True)
def subscribe_to_otc(self, msisdn):
    time.sleep(2)
    se_ussd_response = bet_daily(conf.PID_DAILY, msisdn, 'ussd')
    logging.debug("Response receive %s" % se_ussd_response['errorMsg'])
    product_amount = int(float(se_ussd_response['amount']))
    if se_ussd_response['errorCode'] == "1000":
        message = se_ussd_response['errorMsg']
        get_daily_odds(msisdn)
        insert_user(msisdn, 1, 0)
        insert_cdr(msisdn, 1, se_ussd_response['errorMsg'], product_amount, "USSD")
        insert_logs(msisdn, 1, se_ussd_response['errorMsg'], product_amount)
    else:
        message = se_ussd_response['errorMsg']
        insert_cdr(msisdn, 1, se_ussd_response['errorMsg'], product_amount, "USSD")
        insert_logs(msisdn, 1, se_ussd_response['errorMsg'], product_amount)
        push_sms(msisdn, message)
    logging.debug("Msisdn: %s, Message: %s" % (msisdn, message))


@app.task(bind=True)
def subscribe_to_weekly(self, msisdn):
    time.sleep(2)
    se_ussd_response = bet_weekly(conf.PID_WEEKLY, msisdn, 'ussd')
    product_amount = int(float(se_ussd_response['amount']))
    logging.debug("Response receive %s" % se_ussd_response['errorMsg'])
    insert_cdr(msisdn, 2, se_ussd_response['errorMsg'], product_amount, "USSD")
    insert_logs(msisdn, 2, se_ussd_response['errorMsg'], product_amount)
    if se_ussd_response['errorCode'] == "1000":
        message = "Your request was successful,  sms with odds will be sent shortly."
        insert_user(msisdn, 2, 7)
        insert_cdr(msisdn, 2, se_ussd_response['errorMsg'], product_amount, "USSD")
        insert_logs(msisdn, 2, se_ussd_response['errorMsg'], product_amount)
    else:
        message = se_ussd_response['errorMsg']
        insert_cdr(msisdn, 2, se_ussd_response['errorMsg'], product_amount, "USSD")
        insert_logs(msisdn, 2, se_ussd_response['errorMsg'], product_amount)
    push_sms(msisdn, message)
    logging.debug("Msisdn: %s, Message: %s" % (msisdn, message))
