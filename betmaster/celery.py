import os
from celery import Celery

#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'betmaster.settings')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'betmaster.settings')
app = Celery('betmaster')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()