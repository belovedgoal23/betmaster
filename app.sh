#!/bin/bash
source ~/Envs/pt3/bin/activate
pkill -f 'betmaster.wsgi'

echo starting gunicorn
exec gunicorn betmaster.wsgi -b 0.0.0.0:8091 --daemon \
     --workers=5 -t 2300 \
     --worker-connections 100



