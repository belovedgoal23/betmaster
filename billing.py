from datetime import datetime, date
import logging
import sys
import json
import uuid
import tornado.ioloop
import tornado.web
import tornado.wsgi
import os
import pika
import xml.etree.ElementTree as ET
from datetime import date
import json
from conf import *
from urllib.parse import unquote
import requests
import ast
from daemon import Daemon
from script.Subscription import *


LOG_FORMAT = '%(message)s|%(asctime)s'
log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'subscriber.log')
#LOG_FILE = "/var/log1/billing/worker.log"
pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'run.pid')
logging.basicConfig(filename=log, level=logging.DEBUG)#, format=FORMAT)
DND_URL = "http://smsgw.ly0.co/service/dnd_airtel/%s"


# PID_FILE = '/var/log1/smsgw/SMSG.PID'
#logger = logging.getLogger('betmaster')

#handler = logging.FileHandler(LOG_FILE)
#formatter = logging.Formatter('[%(asctime)s] %(levelname)s %(message)s')
#handler.setFormatter(formatter)
#logger.addHandler(handler)
#logger.setLevel(logging.DEBUG)


def check_dnd(msisdn):
    '''

    :param msisdn:
    :return:
    '''
    url = DND_URL % msisdn
    logging.info(url)
    res = requests.get(DND_URL % msisdn[-10:])
    res.encoding = "utf-8"
    result = json.loads(res.text)
    return result['action']


def sub_request(url, msisdn, keyword):
    '''
    :param url:
    :param msisdn:
    :param keyword:
    :return:
    '''
    msisdn = "234%s" % msisdn[-10:]
    return requests.get(url % (msisdn, keyword))


def sms_kannel(msisdn, message, shortcode):
    '''

    :param msisdn:
    :param message:
    :param shortcode:
    :return:
    '''
    try:
        keyword = message.strip().upper()
        msisdn = msisdn.strip()
        logging.info("Subscription Request: msisdn=%s, Message:%s" % (msisdn, keyword))
        if SUBSCRIPTION.has_key(keyword):
            if keyword in STOP:
                res = sub_request(UNSUB_URL, msisdn, SUBSCRIPTION[keyword])
                #_res = json.loads(res)
                logging.info("response %s" % res.text)
                response = STOP_MESSAGE
            else:
                if not check_dnd(msisdn):
                    res = sub_request(SUB_NEW_URL, msisdn, SUBSCRIPTION[keyword])
                    #logging.info("Response from SE: %s" % res.text)
                    result = json.loads(res.text)
                    sub = ast.literal_eval(result['data'])['errorCode']
                    #logging.info("Response from SE: %s" % sub)
                    if sub == 5021:
                        response = ENTRY_MESSAGE
                    elif sub == 7002:
                        response = AOC_MESSAGE
                    elif sub == 3003:
                        response = PRODUCT_MESSAGE
                    else:
                        response = NOT_COMPLETED
                else:
                    response = BLACKLIST_MESSAGE
        else:
            logging.debug("Invalid keyword")
            response = INVALID_MESSAGE
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))
        return "Request not completed! please try again."

    finally:
        return response


def my_random_string(string_length=6):
    """Returns a random string of length string_length."""
    random = str(uuid.uuid4())
    random = random.upper()
    random = random.replace("-","")
    return 'AT%s' % random[0:string_length]


def process_response(soap_xml):
    '''

    :param soap_xml:
    :return:
    '''
    def soap_process(soap_xml):
        tree = ET.fromstring(soap_xml)
        #for key in keys:
        #    dict[key] = tree[1][0][0].find(key).text
        #dict.update({'status': STATUS[dict['errorMsg']]})
        response = {key: tree[1][0][0].find(key).text for key in keys}
        #response.update({'status': STATUS[response['errorCode']]})
        logging.info(response)
        return response
    return soap_process(soap_xml)


def add_queue(input_data):
    '''

    :param input_data:
    :return:
    '''
    print (input_data)
    credentials = pika.PlainCredentials('bet', 'london')
    connection = pika.BlockingConnection(pika.ConnectionParameters('69.64.81.160', 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue='betmaster')
    channel.basic_publish(exchange='',
                          routing_key='betmaster',
                          body=json.dumps(input_data))
    print(" [x] Sent 'Hello World!'")
    logging.debug("%s with msisdn: %s added to the queue" % (input_data['xactionId'], input_data['msisdn']))
    connection.close()


class MsgHandler(tornado.web.RequestHandler):
    '''

    '''
    # def set_default_headers(self):
    #     print "setting headers!!!"
    #     self.set_header("Access-Control-Allow-Origin", "*")
    #     self.set_header("Access-Control-Allow-Headers", "x-requested-with")
    #     self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

    def post(self):
        logging.debug(self.request.body)
        logging.debug(self.request)
        response = process_response(self.request.body)
        logging.info(response)
        add_queue(response)
        self.write(response['errorMsg'])


class StopHandler(tornado.web.RequestHandler):

    def get(self):
        msisdn = self.get_argument('msisdn')
        action = self.get_argument('action')
        #service_id = self.get_argument('service_id')
        channel = "sms"
        if action.upper() == "STOPBD":
            sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
            xml_sub_res = sub.stop_non_ht(conf.PID_DAILY, msisdn, channel, conf.stop_xml)
            res = process_response(xml_sub_res.text)
            logging.debug(res)
            #self.write("Your request has been received!.")
            if res['errorCode'] == "3016":
                self.write("Betmaster weekly is not active on your line. Dial *20035# to activate.")
            elif res['errorCode'] == "1001":
                self.write("Betmaster weekly has been deactivated on your line.")
        elif action.upper() == "STOPBW":
            sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
            xml_sub_res = sub.stop_non_ht(conf.PID_WEEKLY, msisdn, channel, conf.stop_xml)
            res = process_response(xml_sub_res.text)
            logging.debug(res)
            if res['errorCode'] == "3016":
                self.write("Betmaster weekly is not active on your line. Dial *20035# to activate.")
            elif res['errorCode'] == "1001":
                self.write("Betmaster weekly has been deactivated on your line.")
        else:
            self.write(MESSAGE_STOP)


class SmsHandler(tornado.web.RequestHandler):
    def get(self):
        logging.info(self.request.body)
        msisdn = self.get_argument('msisdn')
        message = self.get_argument('message')
        shortcode = self.get_argument('shortcode')
        res = sms_kannel(msisdn, unquote(message), shortcode)
        self.write(res)


# class TestHandler(tornado.web.RequestHandler):
#     def get(self):
#         logging.info(self.request.body)
#         self.write(res)

#print subscribe("2349029458935", "16")


class SubHandler(tornado.web.RequestHandler):
    def get(self):
        logging.info(self.request.body)
        msisdn = self.get_argument('msisdn')
        service_id = self.get_argument('service_id')
        channel = self.get_argument('channel')
        if msisdn is None:
            self.write("Msisdn is empty")
            exit()
        try:
            if service_id == "8104":
                sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
                xml_sub_res = sub.subscribe_otc(service_id, msisdn, channel, conf.otc_xml)
                res = process_response(xml_sub_res.text)
                logging.debug(res)
            elif service_id == "8103":
                sub = Subscribe(conf.cp_name, conf.cp_id, conf.cp_pwd)
                xml_sub_res = sub.subscribe_non_ht(service_id, msisdn, channel, conf.non_ht_xml)
                res = process_response(xml_sub_res.text)
                logging.debug(res)
            else:
                res = {"Error": "Invalid service id"}
            self.write(json.dumps(res))
        except Exception as ex:
            logging.info(str(ex))
            self.write("An error occurred: %s" % str(ex))


class BillingDaemon(Daemon):
    def run(self):
        main()


def main():
    app = tornado.web.Application(
       [
           #(r"/", EchoHandler),
           (r"/bet/notification/handler", MsgHandler),
            (r"/bet/charging/startstop", StopHandler),
            (r"/bet/kannel/process", SmsHandler),
           (r"/bet/airtel/subscription", SubHandler)
       ])

    app.listen(9999)
    #app.listen(9889)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    daemon = BillingDaemon(pidfile, stdout=log, stderr=log)
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.stop()
            daemon.start()
        else:
            print('unknown command')
            sys.exit(2)
        sys.exit(0)
    else:
        print ('usage: %s start|stop|restart' % sys.argv[0])
        sys.exit(2)
