import redis
import os, sys
import logging
from utility import retry_logs, update_retry
from daemon import Daemon
import requests
from conf import *

fmt = '[%(asctime)s] %(message)s'
log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'retry.log')
#log = '/var/log1/billing/retry.log'
pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'retry.pid')
logging.basicConfig(logfile=log, level=logging.DEBUG, format=fmt)
logger = logging.getLogger("AirtelTT")


KEY= "SUBSCRIPTION_INFORMATION_AOC"
CHOICE = {"true": 1, "false": 0}
PRODUCT = {'5803': '13', '5806': '16', '5797': '7'}


def get_requests(store, key, max_count=1000000):
    count = 0
    while count < max_count:
        line = store.rpop(key)
        if line:
            yield line
            count +=1
        else:
            return


def sub_request(url, msisdn, keyword):
    '''

    :param url:
    :param msisdn:
    :param keyword:
    :return:
    '''
    msisdn = "234%s" % msisdn[-10:]
    res_url = url % (msisdn, keyword)
    logger.debug("New request %s-%s-%s" % (msisdn, keyword, res_url))
    return requests.get(res_url)


def load_records():
    logger.info("Starting to retry")
    result = retry_logs()
    ids = []
    if len(result) > 0:
        for x in result:
            res = sub_request(SUB_NEW, x[1], PRODUCT[x[3]])
            logger.info("Re-Subscribing %s" % res.text)
            logger.info("Testing %s" % x[1])
            ids.append(str(x[0]))
    logger.info(ids)
    new_ids = ",".join(ids)
    update_retry(new_ids)
    logger.info("Completed re-trial for %s" % len(result))


class RetryDaemon(Daemon):
    def run(self):
        load_records()


if __name__ == '__main__':
    daemon = RetryDaemon(pidfile, stdout=log, stderr=log)
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.stop()
            daemon.start()
        else:
            print 'unknown command'
            sys.exit(2)
        sys.exit(0)
    else:
        print 'usage: %s start|stop|restart' % sys.argv[0]
        sys.exit(2)
