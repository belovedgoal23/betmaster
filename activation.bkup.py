'''
author:beloved
'''

from cwc import CWC
from conf import *
import os
import logging
import pika
import json
from urllib.request import urlopen
from urllib.parse import urlencode, quote_plus

periodLength = 7

fmt = '[%(asctime)s] %(message)s'
log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'services.log')
#log = "/var/log1/billing/worker.log"
logging.basicConfig(filename=log, level=logging.DEBUG, format=fmt)
SERVICE = {'8103': '1', '8104': '2'}


def connection():
    con = CWC(PG_HOST, PG_USER, PG_DB, PG_PASSWORD)
    return con.connect_to_pg()


def send_sms(sms_url, short_code, msisdn, msg):
    '''
    Send message to subscriber using short_code
    '''
    to = '234%s' % msisdn[-10:]
    params = {
            'from' :short_code,
            'text': msg,
            'to':'234%s' % msisdn[-10:]
            }
    url = sms_url + urlencode(params)
    try:
        logging.info('%s, invoking: %s' % (msisdn, url))
        response = urlopen(url)
    except Exception as exc:
        logging.error('url: %s, error: %s' % (
            url, str(exc)))
    else:
        logging.info('msisdn: %s, response: %s' % (to, response.read()))


def insert_user(msisdn, service_id,periodLength):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "Insert into Users(MSISDN,SERVICE_ID,FIRST_REG_TIME,LAST_REG_TIME,EFFECTIVE_TIME,EXPIRY_TIME,CHANNEL,STATUS)"  \
              "values('%s','%s',current_timestamp,current_timestamp,current_timestamp,current_timestamp+interval '%s' day, 'API', 1)" % (msisdn, service_id, periodLength)
        logging.debug("Insert user:%s" %  sql)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def reactivate_user(msisdn, service_id, periodLength):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        channel = 'API'
        sql = "update Users set STATUS=1, LAST_REG_TIME = current_timestamp, EFFECTIVE_TIME = current_timestamp," \
              "EXPIRY_TIME = current_timestamp+interval '%s' day, channel='%s' where service_id=%s and msisdn='%s'" % (periodLength, channel, service_id, msisdn)
        logging.debug("Reactivate user: %s" % sql)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("Reactivate user error occured: %s" % str(ex))


def deactivate_user(msisdn, service_id):
    '''

    :param msisdn:
    :param service_id:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "update Users set STATUS=0,LAST_UNREG_TIME = current_timestamp where service_id='%s' and msisdn='%s'" % (service_id, msisdn)
        cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def insert_cdr(msisdn, service_id, reason, price, channel):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        result = 1
        description = 'SuccessFul'
        sql = "insert into cdr(service_id,charge_time,msisdn,price,reason,channel,result,description) " \
              "values('%s',current_timestamp,'%s','%s','%s','%s','%s','%s')" % (service_id, msisdn, price, reason, channel, result, description)
        logging.info("Insert CDR: %s" % sql)
        res = cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("Insert CDR ERROR: %s" % str(ex))


def insert_logs(msisdn, service_id, reason, price):
    '''

    :param msisdn:
    :param service_id:
    :param reason:
    :param price:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        result = 1
        description = 'SuccessFul'
        sql = "insert into register_logs(service_id,msisdn,request_type,message,channel,result,description,created_time,updated_time)" \
              " values('%s', '%s', '%s', '%s', '%s', '%s', '%s',current_timestamp,current_timestamp)" % (service_id, msisdn, 1, 1, "", result, description)
        logging.info("InsertLog: %s" % sql)
        res  = cursor.execute(sql)
        con.commit()
    except Exception as ex:
        logging.error("An error occured: %s" % str(ex))


def check_exist(service_id, msisdn):
    '''

    :param service_id:
    :param msisdn:
    :return:
    '''
    try:
        con = connection()
        cursor = con.cursor()
        sql = "Select status from users where service_id = '%s' and msisdn = '%s'" % (service_id, msisdn)
        logging.info("Check Exist Sql: %s" % sql)
        cursor.execute(sql)
        res = cursor.fetchone()
        if res is None:
            return False
        return True
    except Exception as ex:
        logging.error("Check exist error occured: %s" % str(ex))
