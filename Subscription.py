import requests
import conf
from tornado import template
import logging, os

fmt = '[%(asctime)s] %(message)s'
log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'subscription.log')
#log = '/var/log1/billing/request.log'
pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'run.pid')
logging.basicConfig(filename=log, level=logging.DEBUG, format=fmt)


# logging.basicConfig(format=FORMAT)

class Subscribe(BaseException):

    def __init__(self, cp_name, cp_id, cp_password):
        """

        :param cp_name:
        :param cp_id:
        :param cp_password:
        """
        self.cp_name = cp_name
        self.cp_password = cp_password
        self.cp_id = cp_id


    def format_msisdn(self, msisdn):
        return '234%s' % msisdn[-10:]

    def add_header(self):
        """

        :return:
        """
        return {"Content-Type": "application/xml",
                "SOAPAction": "handleNewSubscription"}

    def subscribe_otc(self, pid, msisdn, channel, otc_xml):
        """

        :param pid:
        :param msisdn:
        :param channel:
        :return:
        """
        request_xml = template.Template(otc_xml)
        #payload = request_xml.generate({"cpid": self.cp_id, "pwd":self.cp_password, "cpname": self.cp_name, "channel": channel,"msisdn": msisdn, "pid": pid})
        payload = request_xml.generate(cpid=self.cp_id, pwd =self.cp_password,cpname=self.cp_name,channel=channel,msisdn= self.format_msisdn(msisdn), product_id=pid)
        logging.debug("%s" % str(payload))
        response = requests.post(conf.SE_URL, data=payload, headers=self.add_header(), verify=False)
        return response

    def subscribe_non_ht(self, pid, msisdn, channel, non_ht_xml):
        """

        :param pid:
        :param msisdn:
        :param channel:
        :return:
        """
        request_xml = template.Template(non_ht_xml)
        payload = request_xml.generate(cpid=self.cp_id, pwd=self.cp_password, cpname=self.cp_name, channel=channel, msisdn= self.format_msisdn(msisdn), product_id=pid)
        logging.debug("%s" % str(payload))
        response = requests.post(conf.SE_URL, data=payload, headers=self.add_header(), verify=False)
        return response

    def stop_non_ht(self, pid, msisdn, channel, stop_xml):
        """

        :param pid:
        :param msisdn:
        :param channel:
        :return:
        """
        request_xml = template.Template(stop_xml)
        payload = request_xml.generate(cpid=self.cp_id, pwd=self.cp_password, cpname=self.cp_name, channel=channel, msisdn= self.format_msisdn(msisdn), product_id=pid)
        logging.debug("%s" % str(payload))
        response = requests.post(conf.SE_URL, data=payload, headers=self.add_header(), verify=False)
        return response



