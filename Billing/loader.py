import redis
import os, sys
import logging
from utility import insert_retry
from Billing.daemon import Daemon

fmt = '[%(asctime)s] %(message)s'
#log = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'request.log')
log = '/var/log1/billing/request.log'
pidfile = os.path.join(os.path.abspath(os.path.dirname(os.path.abspath(__file__))), 'retry.pid')
logging.basicConfig(logfile=log, level=logging.DEBUG, format=fmt)

KEY= "SUBSCRIPTION_INFORMATION_AOC"

CHOICE = {"true": 1, "false": 0}


def get_requests(store, key, max_count=1000000):
    count = 0
    while count < max_count:
        line = store.rpop(key)
        if line:
            yield line
            count +=1
        else:
            return


def load_records():
    store = redis.Redis()
    for line in get_requests(store, KEY):
        try:
            msisdn, product ,act_date, status = line.split("|")
            insert_retry(msisdn, product, act_date, CHOICE[status])
        except Exception, ex:
            logging.error("An error occured! please try again later")
            store.lpush(KEY, line)


class RetryDaemon(Daemon):
    def run(self):
        load_records()


if __name__ == '__main__':
    daemon = RetryDaemon(pidfile, stdout=log, stderr=log)
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.stop()
            daemon.start()
        else:
            print 'unknown command'
            sys.exit(2)
        sys.exit(0)
    else:
        print 'usage: %s start|stop|restart' % sys.argv[0]
        sys.exit(2)
