#import xmltodict
import untangle
xml ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wsa="http://www.w3.org/2005/08/addressing"><soapenv:Header><wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">http://172.24.18.101:9234/NotificationSE.php</wsa:To><wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">notificationToCP</wsa:Action><wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">uuid:972A6F56-0165-4000-E000-DDF7AC180615</wsa:MessageID></soapenv:Header><soapenv:Body><p598:notificationToCP xmlns:p598="http://SubscriptionEngine.ibm.com"><notificationRespDTO><amount>20.0</amount><chargigTime>2018-09-01T22:04:25.000Z</chargigTime><errorCode>1000</errorCode><errorMsg>AOC: Success</errorMsg><lowBalance>0.0</lowBalance><msisdn>2348025440521</msisdn><productId>6300</productId><temp1>39</temp1><temp2>10177270401</temp2><temp3>&lt;XML&gt;&lt;ID&gt;65460545&lt;/ID&gt;&lt;ChargingType&gt;N&lt;/ChargingType&gt;&lt;VCode&gt;null&lt;/VCode&gt;&lt;partyB&gt;null&lt;/partyB&gt;&lt;/XML&gt;</temp3><xactionId>1587335809</xactionId></notificationRespDTO></p598:notificationToCP></soapenv:Body></soapenv:Envelope>'


xmll = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
   <soapenv:Header>
      <wsa:To>http://172.24.18.101:9234/NotificationSE.php</wsa:To>
      <wsa:Action>notificationToCP</wsa:Action>
      <wsa:MessageID>uuid:972A6F56-0165-4000-E000-DDF7AC180615</wsa:MessageID>
   </soapenv:Header>
   <soapenv:Body>
      <p598:notificationToCP xmlns:p598="http://SubscriptionEngine.ibm.com">
         <notificationRespDTO>
            <amount>20.0</amount>
            <chargigTime>2018-09-01T22:04:25.000Z</chargigTime>
            <errorCode>1000</errorCode>
            <errorMsg>AOC: Success</errorMsg>
            <lowBalance>0.0</lowBalance>
            <msisdn>2348025440521</msisdn>
            <productId>6300</productId>
            <temp1>39</temp1>
            <temp2>10177270401</temp2>
            <temp3>&lt;XML&gt;&lt;ID&gt;65460545&lt;/ID&gt;&lt;ChargingType&gt;N&lt;/ChargingType&gt;&lt;VCode&gt;null&lt;/VCode&gt;&lt;partyB&gt;null&lt;/partyB&gt;&lt;/XML&gt;</temp3>
            <xactionId>1587335809</xactionId>
         </notificationRespDTO>
      </p598:notificationToCP>
   </soapenv:Body>
</soapenv:Envelope>
"""

#obj = untangle.parse(xmll)

#print obj.soapenv_Envelope.soapenv_Body.p598_notificationToCP.notificationRespDTO.productId
#

keys = ('amount',
			'chargigTime',
			'errorCode',
			'errorMsg',
			'lowBalance',
			'msisdn',
			'productId',
			'xactionId',
			'temp1'
        )

import xml.etree.ElementTree as ET
tree = ET.fromstring(xml)


print  tree[1][0][0].find('errorMsg').text


#
# STATUS = {'AOC: Success':'REG', 'Success': 'RENEW', 'Response received AOC::Time Out': 'TIMEOUT', 'Successful Deprovisioning':'STOP'}
#
# keys = (    'amount',
# 			'chargigTime',
# 			'errorCode',
# 			'errorMsg',
# 			'lowBalance',
# 			'msisdn',
# 			'productId',
# 			'xactionId',
# 			'temp1'
#         )
#
# def process_response(soap_xml):
#     dict = {}
#
#     def soap_process(soap_xml):
#         tree = ET.fromstring(soap_xml)
#         for key in keys:
#             dict[key] = tree[1][0][0].find(key).text
#         dict.update({'status': STATUS[dict['errorMsg']]})
#         return dict
#     return soap_process(soap_xml)
#
# print process_response(xmll)['status']
#
# # for child in tree[1][0][0]:
# #     print  child.tag, child.text
# #     #print child.tag, child.attrib
#
# #print tree.text()

# dict = {}
# for key in keys:
#     res = tree[1][0][0].find(key).text
#     dict[key] = res
#
# print(dict)
