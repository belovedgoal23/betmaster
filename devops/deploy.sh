#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no root@69.64.81.33 << 'ENDSSH'
 cd /root/projects
 docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 docker pull registry.gitlab.com/beloved23/betmaster:latest
 docker-compose up -d
ENDSSH